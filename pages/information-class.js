import React from 'react'
import Layout from './layout'

function Information(props) {

    return (
        <>
            <Layout>
                <div className="container">
                    <div className="c-title-blog">
                        <a href="#">All Posts</a>
                    </div>
                    <div className="c-information-class">
                        <div className="c-title-class">
                            Stay fit and committed by finding an activity you enjoy
                        </div>
                        <div className="c-content-class">
                            <p>To create and manage your own content, open the <b>Blog Manager</b> by hovering over your blog feed and clicking <b>Manage</b>. Here you can create, edit and delete posts and manage categories. You can also update your post settings and SEO, duplicate or draft posts, turn off commenting, or delete a post altogether by clicking <b>Edit</b> on each blog post.<br/><br/>

                            To delete or edit an existing image or video in each post, click on the media to reveal a toolbar, which also allows you to customize the size and layout of your visuals.<br/><br/>

                            Add more elements to your post by clicking on each of the symbols at the bottom of your post. Insert an image or gallery, embed HTML, or add a GIF to spice up your content.<br/><br/>

                            Add a cover photo to your post before publishing by clicking <b>Settings</b> on the left sidebar. Your cover photo is visible to all users who browse the blog on your site. Edit how your posts show up on search results and make them more discoverable by editing the SEO for each post.<br/><br/>

                            Add categories to your posts so users can navigate your blog pages by topic. Once you’re satisfied with your post, go live by clicking <b>Publish</b>.<br/><br/></p>
                        </div>
                        <div className="c-img-class">
                        <img src="http://golftimes.vn/wp-content/uploads/2016/10/Vinpearl-da-nang-nhung-san-golf-dep-nhat-viet-nam.jpg" alt=""/>
                        </div>
                    </div>
                </div>
            </Layout>
        </>
    )

} export default Information;
