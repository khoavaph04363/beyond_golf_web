import Layout from './layout';

function Faq() {
    return (
      <>
        <Layout>
          <div className="c-block-text">
            <div className="container">
                <div className="c-textbox">Everything You Need to Know</div>
                <h3>You’ve Got Questions - I’ve Got Answers</h3>
            </div>
          </div>
          <div className="c-faq-text">
            <div className="container">
              <ul>
                  <li>
                    <span>Do I need to be at a certain athletic level to train with beyond golf?</span>
                    <span>Enter your answer here. Be thoughtful with your answer, write clearly, and consider adding examples.</span>
                  </li>
                  <li>
                      <span>How is Jon Doe different from other trainers?</span>
                      <span>Enter your answer here. Be thoughtful with your answer, write clearly, and consider adding examples.</span>
                  </li>
                  <li>
                      <span>Has Jon Doe worked with any professional athletes?</span>
                      <span>Enter your answer here. Be thoughtful with your answer, write clearly, and consider adding examples.</span>
                  </li>
              </ul> 
            </div>
          </div>
        </Layout>
      </>
    )
}
export default Faq;