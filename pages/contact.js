import React from 'react';
import Contact from '../components/Contact';
import Layout from './layout';

function PageContact(props) {
    return (
        <>
            <Layout>
                <Contact/>
            </Layout>
        </>
    )

}

export default PageContact;