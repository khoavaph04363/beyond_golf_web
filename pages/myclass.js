import React from 'react'
import NewList from '../components/NewList'
import Layout from './layout';

function MyClass(props) {

    return (
        <>  
        <Layout>
            <div className="container">
                <div className="c-strong-title">
                    <span>My Classes</span>
                    <div className="c-strong-title__myclass">
                        Train for Success
                    </div>
                </div>
                <NewList/>
            </div>
            </Layout>
        </>
    )

} export default MyClass;
