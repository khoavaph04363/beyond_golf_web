import React, { useEffect } from 'react';
import Link from 'next/link'

var menuMobileExpand = function () {
    var menu = $(".c-menu");
    var parent = $(this).parent();
    if (menu.hasClass("has-menu-tiny")) {
        $(parent).removeClass("is-active");
        $(this).removeClass("is-active");
        menu.removeClass("has-menu-tiny").slideUp();
    } else {
        $(parent).addClass("is-active");
        $(this).addClass("is-active");
        menu.addClass("has-menu-tiny").slideDown();
    }
};
function Header() {

    useEffect(() => {
        $('.c-menu .c-submenu-btn').click(function (e) {
            e.preventDefault();
            var grand = $(this).parent().parent();
            var root = $(this).parent().parent().parent();
            if ($(this).hasClass('fa-angle-down')) {
                $(this).removeClass('fa-angle-down');
                $(this).addClass('fa-angle-up');
                $('li.mobile-active > .c-dropdown-menu', $(root)).slideUp();
                $('li.mobile-active > a > .c-submenu-btn', $(root)).removeClass('fa-angle-up');
                $('li.mobile-active > a > .c-submenu-btn', $(root)).addClass('fa-angle-down');
                $('li.mobile-active', $(root)).removeClass('mobile-active').addClass('mobile-hidden');
                $(grand).addClass('mobile-active').removeClass('mobile-hidden');
                $('>.c-dropdown-menu', $(grand)).slideDown();
            } else {
                $(this).removeClass('fa-angle-up');
                $(this).addClass('fa-angle-down');
                $(grand).removeClass('mobile-active').addClass('mobile-hidden');
                $('>.c-dropdown-menu', $(grand)).slideUp();
            }
        });
    }, [])

    return (
        <>
            <div className="l-nav">
                <div className="container clearfix">
                    <div className="c-logo">
                        <Link href="/">
                            <a>
                                <img src="logo.webp" alt="" />
                            </a>
                        </Link>
                    </div>
                    <button
                        className="c-menu-expand js-menu-expand"
                        type="button"
                        onClick={menuMobileExpand}
                    >
                        <span></span>
                    </button>
                    <div className="c-menu">
                        <ul className="clearfix">
                            <li><Link href="#"><a>Home</a></Link></li>
                            <li className="c-menu-child">
                                <Link href="#"><a>Về Beyond Golf<span className="c-submenu-btn fa fa-angle-down"></span></a></Link>
                                <div className="c-dropdown-menu">
                                    <ul>
                                        <li><Link href="/#"><a>Giới thiệu chung</a></Link></li>
                                    </ul>
                                </div>
                            </li>
                            <li><Link href="/blog"><a>First Tee</a></Link></li>
                            <li><Link href="#"><a>Ngoại giao golf</a></Link></li>
                            <li><Link href="#"><a>Hội viên BG</a></Link></li>
                            <li><Link href="#"><a>Khóa học</a></Link></li>
                            <li><Link href="/contact"><a>More</a></Link></li>
                            <li><div className="c-login"><span><i className="fa fa-user"></i>&nbsp;Log In</span></div></li>
                        </ul>
                    </div>
                </div>
            </div>
        </>
    );

}

export default Header;