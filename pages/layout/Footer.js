import react from 'react';

function Footer() {
    return (
        <>
            <div className="l-footer">
                <div className="container">
                    <div className="c-all-footer">
                    <ul className="clearfix">
                        <li>
                            <div className="c-logo-footer">
                            <a href="/"><img src="logo.webp"/></a>
                            </div>
                        </li>
                        <li>
                            <div className="c-company-address">
                                <span>Address</span>
                                <span>Hanoi, Vietnam</span>
                            </div>
                        </li>
                        <li>
                            <div className="c-company-contact">
                                <span>Contact</span>
                                <span>beyondgolfvietnam@gmail.com</span>
                                <span>123-456-7890</span>
                            </div>
                        </li>
                        <li>
                            <span>Follow</span>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Footer;