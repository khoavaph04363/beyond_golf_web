import React from 'react';
import Head from 'next/head'
import Header from './Header';
import Footer from './Footer';

function Layout({
    children,
    title = 'Beyond Golf',
    image = 'http://128.199.96.181:4000/uploads/main.jpg',
    des = 'A Golf Instructor For All Levels'
}) {
    return (
        <>
            <Head>
                <title>{title}</title>
                <link rel="icon" href="/favicon.ico" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta property="og:title" content={title} />
                <meta property="og:description" content={des} />
                <meta property="og:image" content={image} />
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
                <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
                <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
                <script src="https://unpkg.com/scrollreveal"></script>
            </Head>
            <Header/>
            {children}
            <Footer/>
        </>
    );

}

export default Layout;