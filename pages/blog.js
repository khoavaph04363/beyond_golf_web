import React, { Component } from 'react'
import Layout from './layout'
import Link from 'next/link'

function Blog(props) {

    return (
        <>
            <Layout>
                <div className="container">
                    <div className="c-title-blog">
                        <a href="#">All Posts</a>
                    </div>

                    <div className="c-content-blog">
                        <ul className="clearfix">
                            <li>
                                
                                <div className="c-left-blog">
                                <Link href="/information-class"><a > <img src="http://golftimes.vn/wp-content/uploads/2016/10/Vinpearl-da-nang-nhung-san-golf-dep-nhat-viet-nam.jpg" alt=""/></a></Link>
                                </div>
                                <div className="c-right-blog">
                                    <div className="c-title-right">
                                    <Link href="/information-class"><a>Stay fit and committed by finding an activity you enjoy</a></Link>
                                    </div>
                                    <div className="c-content-right">
                                    To create and manage your own content, open the Blog Manager by hovering over your blog feed and clicking Manage. Here you can create, edit and delete posts and manage categories.
                                    </div>
                                </div>
                                
                            </li>
                            <li>
                                
                                <div className="c-left-blog">
                                <Link href="/information-class"><a > <img src="http://golftimes.vn/wp-content/uploads/2016/10/Vinpearl-da-nang-nhung-san-golf-dep-nhat-viet-nam.jpg" alt=""/></a></Link>
                                </div>
                                <div className="c-right-blog">
                                    <div className="c-title-right">
                                    <Link href="/information-class"><a>Stay fit and committed by finding an activity you enjoy</a></Link>
                                    </div>
                                    <div className="c-content-right">
                                    To create and manage your own content, open the Blog Manager by hovering over your blog feed and clicking Manage. Here you can create, edit and delete posts and manage categories.
                                    </div>
                                </div>
                                
                            </li>
                            <li>
                                
                                <div className="c-left-blog">
                                <Link href="/information-class"><a > <img src="http://golftimes.vn/wp-content/uploads/2016/10/Vinpearl-da-nang-nhung-san-golf-dep-nhat-viet-nam.jpg" alt=""/></a></Link>
                                </div>
                                <div className="c-right-blog">
                                    <div className="c-title-right">
                                    <Link href="/information-class"><a>Stay fit and committed by finding an activity you enjoy</a></Link>
                                    </div>
                                    <div className="c-content-right">
                                    To create and manage your own content, open the Blog Manager by hovering over your blog feed and clicking Manage. Here you can create, edit and delete posts and manage categories.
                                    </div>
                                </div>
                                
                            </li>
                        </ul>

                    </div>
                </div>
            </Layout>
        </>
    )

}
export default Blog;
