import request from './request';
let dataService = {
  // getAvailabilityByHotels: params => {
  //   let url = 'api/hotels/availability-by-hotels';
  //   return request.post(params, url);
  // },
  ///api/post/get-by-category
  getByCategory: (params) => {
    let url = 'api/post/get-by-category';
    return request.post(params, url);
  },
  getListBanner: (params) => {
    let url = 'api/banner/get-all-banners';
    return request.post(params, url);
  },
  getByHome: (params) => {
    let url='api/post/get-by-home';
    return request.post(params,url);
  },
  getCategory: (params) =>{
    let url ='api/category/get-all';
    return request.post(params,url);
  },
  createFeedBack: (params) =>{
    let url ='api/feedback/create-feedback';
    return request.post(params, url);
  },
  getNewDetail: (params) => {
    let url ='api/post/get-by-id';
    return request.post(params, url);
  },
  getAllPost: (params) => {
    let url ='api/post/get-all';
    return request.post(params, url);
  },
  getAllBanner: (params) => {
    let url ='api/banner/get-all-banner';
    return request.post(params, url);
  }
};

export default dataService;
