import Layout from './layout';

function AboutUs() {
    return (
      <>
        <Layout>
            <div className="c-block">
                <div className="container">
                    <div className="c-about-us">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="c-about-image">
                                    <img src='images/about-us.png'/>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="c-about-content">
                                    <h1>My Story</h1>
                                    <span>
                                        As a professional Golf Instructor, I have become well-known and respected throughout 
                                        the greater San Francisco area. I have developed my own unique training methods, 
                                        all aimed at syncing the body and mind in preparation for competing against the best. 
                                        I believe that a rigorous training system not only develops physical strength, 
                                        but also the mental stamina required to compete at the highest levels of the game. 
                                        At Beyond Golf, I keep up with the latest training techniques and methodologies, 
                                        and consistently push my athletes to the limit.
                                    </span>
                                    <div className="c-about-content__schedule">
                                        <a href="/">Schedule a Session</a>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
      </>
    )
}
export default AboutUs;
