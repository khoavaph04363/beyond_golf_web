import React, { useState } from 'react'
import OtpInput from 'react-otp-input';

function Login(props) {
    const [otp, setOtp] = useState("");
    const getOtp = e => {
        setOtp(e)
    }

    return (
        <>
            <div className="container">
                <div className="c-title-login">
                    <h2>Sign Up</h2>
                    <p>Already a member?</p>
                </div>
                <div className="c-import-phone">
                    <div className="form-group">
                        <input type="phone" class="form-control c-text-phone" placeholder="Nhập số điện thoại" />
                    </div>
                </div>
                <div className="c-change-point-box__form clearfix">
                <div className="c-title-otp">
                        <p>Nhập Mã OTP</p>
                    </div>
                    <div className="c-otp-link">
                        <OtpInput
                            value={otp}
                            onChange={(e) => getOtp(e)}
                            numInputs={4}
                            separator={<span>&nbsp;&nbsp;</span>}
                        />
                    </div>
                   
                </div>
                <div className="c-btn-login">
                    <button type="submit" class="btn btn-primary mb-2 c-btn-build">Đăng Ký</button>
                </div>
            </div>
        </>
    )

} export default Login;

