import { Parallax } from 'react-parallax';
import ClassesWeOffer from '../components/ClassesWeOffer';
import Event from '../components/Event'
import Layout from './layout';
import LogoList from '../components/LogoList';
import OurStrengths from '../components/OurStrengths';
import Contact from '../components/Contact';
import NewList from '../components/NewList';

function Home() {
  return (
    <>
      <Layout title={"Home - Beyond Golf"}>
        <div className="c-banner">
          <Parallax className="c-banner-parallax" blur={{ min: -15, max: 15 }} bgImage="https://img.wallpapersafari.com/desktop/1280/1024/66/92/G4sBWT.jpg" bgImageAlt="the cat" strength={200}>
            <div className="c-banner-parallax__content">
              <h1>Beyond Golf Vietnam</h1>
              <span>"Beyond golf's expectations"</span>
            </div>
          </Parallax>
        </div>
        <div className="c-image-text-b">
          <img src="images/home-banner-2.webp" />
          <div className="container">
            <div className="c-image-text-b__content">
              <h2>HỌC VIỆN BEYOND GOLF VIỆT NAM</h2>
              <b>"HƠN CẢ MONG ĐỢI CỦA GOLF"</b>
              <p>Thành lập vào năm 2020, Beyond Golf cung cấp các dịch vụ như: đào tạo, tổ chức các giải Golf, thẻ tích điểm liên kết khi chơi Golf tại các sân Golf và driving range… Đồng thời, kết hợp với các đối tác trong nước và quốc tế phổ biến văn hóa Golf, văn hóa kinh doanh, tạo môi trường giao lưu, hợp tác giữa các nhà ngoại giao, doanh nhân và người yêu Golf. Hơn nữa, Beyond Golf, với tư cách là đại diện duy nhất tại Việt Nam của PGA TourFirstTee Foundation – Liên hiệp Golf phi lợi nhuận cho Trẻ em</p>
              <a href="/#">HOTLINE: +0123456789</a>
            </div>
          </div>
        </div>
        <div className="c-image-home-l">
          <div className="row">
            <div className="col-md-6 is-list">
              <ul className="clearfix">
                <li>
                  <img src="images/home-logo-1.jpg" />
                  <br />
                  <span>First Tee</span>
                </li>
                <li>
                  <img src="images/home-logo-2.jpg" />
                  <br />
                  <span>Ngoại giao Golf</span>
                </li>
                <li>
                  <img src="images/home-logo-3.jpg" />
                  <br />
                  <span>HỘI VIÊN BG</span>
                </li>
                <li>
                  <img src="images/home-logo-4.jpg" />
                  <br />
                  <span>KHÓA HỌC</span>
                </li>
              </ul>
            </div>
            <div className="col-md-6 is-img-full">
              <img src="images/home-banner-3.webp" />
            </div>
          </div>
        </div>
        <LogoList />
        <OurStrengths/>
        <Contact/>
        <NewList/>
      </Layout>
    </>
  )
}
export default Home;