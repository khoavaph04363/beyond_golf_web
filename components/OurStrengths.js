import React from 'react'

function OurStrengths(props) {

    return (
        <>
            <div className="container">
                <div className="c-strong-title">
                    <span>Thế mạnh của chúng tôi</span>
                </div>
                <div className="c-all-content">
                <div className="c-col">
                    <div className="row">

                        <div className="col-md-6">
                            <div className="c-strong__left">
                                <div className="c-img-strong">
                                    <img src="images/strong1.webp" alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="c-strong__right">
                                <div className="c-content-strong">
                                    <p>Gold hòa quyện với ngoại giao</p>
                                    <span>Tập luyện Golf và học về văn hóa ngoại giao với những huấn luyện viên Hàn Quốc, Thái Lan.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="c-col">
                    <div className="row">

                        <div className="col-md-6">
                            <div className="c-strong__left">
                                <div className="c-img-strong">
                                    <img src="images/strong2.webp" alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 ">
                            <div className="c-strong__right">
                                <div className="c-content-strong">
                                    <p>Golf và trách nhiệm xã hội</p>
                                    <span>Với tư cách là đại diện duy nhất tại Việt Nam của PGA TourFirstTee Foundation – Liên hiệp Golf phi lợi nhuận cho Trẻ em, thực hiện các dự án phi lợi nhuận hỗ trợ trẻ em Việt Nam tiếp cận với môn thể thao này.</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="c-col">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="c-strong__left">
                                <div className="c-img-strong">
                                    <img src="images/strong3.webp" alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="c-strong__right">
                                <div className="c-content-strong">
                                    <p>Thẻ hội viên BeyondGolf - Golf Alliance</p>
                                    <span>Thẻ hội viên tích điểm của BeyondGolf cho phép thành viên sử dụng điểm để có được những discount tại nhiều sân golf tại Việt Nam</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </>
    )

} export default OurStrengths;
