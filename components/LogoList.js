import React from 'react';
import Slider from "react-slick";

function NextArrow(props) {
    const { onClick } = props;
    return (
        <div onClick={onClick} className="c-partner-list__right">
            <i className="fa fa-chevron-right"></i>
        </div>
    );
}
function PreviousArrow(props) {
    const { onClick } = props;
    return (
        <div onClick={onClick} className="c-partner-list__left">
            <i className="fa fa-chevron-left"></i>
        </div>
    );
}

function LogoList(props) {
    const settings = {
        autoplay: true,
        infinite: false,
        slidesToShow: 1,
        speed: 500,
        rows: 1,
        slidesPerRow: 1,
        nextArrow: <NextArrow />,
        prevArrow: <PreviousArrow />
    };
    return (
        <>
            <div className="c-partner-list">
                <div className="container">
                    <div className="c-partner-list__title">
                        <h2>ĐỐI TÁC CHIẾN LƯỢC</h2>
                    </div>
                    <div className="c-partner-list__slider">
                        <ul className="clearfix">
                            <Slider {...settings}>
                                <li>
                                    <div className="c-partner-list__item">
                                        <img src="images/partner.webp" />
                                    </div>
                                </li>
                                <li>
                                    <div className="c-partner-list__item">
                                        <img src="images/partner.webp" />
                                    </div>
                                </li>
                                <li>
                                    <div className="c-partner-list__item">
                                        <img src="images/partner.webp" />
                                    </div>
                                </li>
                            </Slider>
                        </ul>
                    </div>
                </div>
            </div>
        </>
    );

}

export default LogoList;