import React from 'react'

function Contact(props) {
    return (
        <>
        <div className="c-background-contact" style={{backgroundImage:"url('images/backgroup-contact.webp')"}}>
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <div className="c-all-left">
                            <div className="c-group-left">
                                <div className="c-title-left">
                                    <span>Liên hệ với chúng tôi</span>
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control c-build-text" placeholder="Tên" />
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control c-build-text" placeholder="Email" />
                                </div>
                                <textarea className="form-control c-build-commit" placeholder="Nhập nội dung tin nhắn của bạn..." rows="4 " />
                                <button type="submit" className="btn btn-primary mb-2 c-btn-build">Submit</button>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="c-img-contact">
                            <img src="images/contact.webp" alt="" />
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </>
    )

} export default Contact;
