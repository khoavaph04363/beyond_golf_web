function Event() {
    return (
      <>
        <div className="c-event">
          <div className="container">
            <ul className="clearfix">
              <li>
                <div className="c-event-time clearfix">
                  <div className="c-event-time__day">12</div>
                  <div className="c-event-time__month">Feb</div>
                </div>
                <div className="c-event-content clearfix" >
                  <div className="c-event-content__title">The Venue</div>
                  <div className="c-event-content__address">Los Angeles, CA</div>
                  <div className="c-event-text">
                    This is your upcoming show description. Use this space to get your fans excited
                    and eager to attend by providing more details like where it's located, 
                    when it begins and ends, what the ticket prices are, and any other special 
                    instructions.
                  </div>
                </div>
                <div className="c-event-notify">
                  <a href="/">Notify Me</a>
                </div>
              </li>
              <li>
                <div className="c-event-time clearfix">
                  <div className="c-event-time__day">14</div>
                  <div className="c-event-time__month">Feb</div>
                </div>
                <div className="c-event-content clearfix" >
                  <div className="c-event-content__title">The Venue</div>
                  <div className="c-event-content__address">Los Angeles, CA</div>
                  <div className="c-event-text">
                    This is your upcoming show description. Use this space to get your fans excited
                    and eager to attend by providing more details like where it's located, 
                    when it begins and ends, what the ticket prices are, and any other special 
                    instructions.
                  </div>
                </div>
                <div className="c-event-notify">
                  <a href="/">Notify Me</a>
                </div>
              </li> 
              <li>
                <div className="c-event-time clearfix">
                  <div className="c-event-time__day">16</div>
                  <div className="c-event-time__month">Feb</div>
                </div>
                <div className="c-event-content clearfix" >
                  <div className="c-event-content__title">The Venue</div>
                  <div className="c-event-content__address">Los Angeles, CA</div>
                  <div className="c-event-text">
                    This is your upcoming show description. Use this space to get your fans excited
                    and eager to attend by providing more details like where it's located, 
                    when it begins and ends, what the ticket prices are, and any other special 
                    instructions.
                  </div>
                </div>
                <div className="c-event-notify">
                  <a href="/">Notify Me</a>
                </div>
              </li>
              <li>
                <div className="c-event-time clearfix">
                  <div className="c-event-time__day">18</div>
                  <div className="c-event-time__month">Feb</div>
                </div>
                <div className="c-event-content clearfix" >
                  <div className="c-event-content__title">The Venue</div>
                  <div className="c-event-content__address">Los Angeles, CA</div>
                  <div className="c-event-text">
                    This is your upcoming show description. Use this space to get your fans excited
                    and eager to attend by providing more details like where it's located, 
                    when it begins and ends, what the ticket prices are, and any other special 
                    instructions.
                  </div>
                </div>
                <div className="c-event-notify">
                  <a href="/">Notify Me</a>
                </div>
              </li> 
              <li>
                <div className="c-event-time clearfix">
                  <div className="c-event-time__day">20</div>
                  <div className="c-event-time__month">Feb</div>
                </div>
                <div className="c-event-content clearfix" >
                  <div className="c-event-content__title">The Venue</div>
                  <div className="c-event-content__address">Los Angeles, CA</div>
                  <div className="c-event-text">
                    This is your upcoming show description. Use this space to get your fans excited
                    and eager to attend by providing more details like where it's located, 
                    when it begins and ends, what the ticket prices are, and any other special 
                    instructions.
                  </div>
                </div>
                <div className="c-event-notify">
                  <a href="/">Notify Me</a>
                </div>
              </li> 
              <li>
                <div className="c-event-time clearfix">
                  <div className="c-event-time__day">22</div>
                  <div className="c-event-time__month">Feb</div>
                </div>
                <div className="c-event-content clearfix" >
                  <div className="c-event-content__title">The Venue</div>
                  <div className="c-event-content__address">Los Angeles, CA</div>
                  <div className="c-event-text">
                    This is your upcoming show description. Use this space to get your fans excited
                    and eager to attend by providing more details like where it's located, 
                    when it begins and ends, what the ticket prices are, and any other special 
                    instructions.
                  </div>
                </div>
                <div className="c-event-notify">
                  <a href="/">Notify Me</a>
                </div>
              </li>
              <li>
                <div className="c-event-time clearfix">
                  <div className="c-event-time__day">25</div>
                  <div className="c-event-time__month">Feb</div>
                </div>
                <div className="c-event-content clearfix" >
                  <div className="c-event-content__title">The Venue</div>
                  <div className="c-event-content__address">Los Angeles, CA</div>
                  <div className="c-event-text">
                    This is your upcoming show description. Use this space to get your fans excited
                    and eager to attend by providing more details like where it's located, 
                    when it begins and ends, what the ticket prices are, and any other special 
                    instructions.
                  </div>
                </div>
                <div className="c-event-notify">
                  <a href="/">Notify Me</a>
                </div>
              </li>   
            </ul>
          </div>
        </div>
      </>
    )
}
export default Event;