import React from 'react'

function NewList(props) {

    return (
        <>
            <div className="container">
                <div className="c-new-list">
                    <ul className="clearfix">
                        <li>
                            <div className="c-img-list">
                                <a href="/#"><img src="images/strong1.webp" />                       
                                <span>Stay fit and committed by finding an activity you...</span></a>           
                            </div>
                        </li>
                        <li>
                            <div className="c-img-list">
                                <a href="/#"><img src="images/strong1.webp" />                       
                                <span>Stay fit and committed by finding an activity you...</span></a>           
                            </div>
                        </li>
                        <li>
                            <div className="c-img-list">
                                <a href="/#"><img src="images/strong1.webp" />                       
                                <span>Stay fit and committed by finding an activity you...</span></a>           
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </>
    )

} export default NewList;
