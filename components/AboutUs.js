function AboutUs() {
    return (
      <>
        <div className="c-block-text">
          <div className="container">
            <div className="c-textbox">About Us</div>
            <h3>Your Golf Instructor</h3>
            <span>
                Based in the greater San Francisco area, I have been training athletes since 2000. 
                I am extremely passionate about being active, healthy and developing the proper techniques. 
                But I’m also extremely passionate about the game and ready to help my clients improve theirs. 
                I’ve been told I’m tough but understanding, and I give each player the individual guidance they need 
                to exceed their potential.
            </span>
          </div>   
        </div>
        <div className="c-image-text">
          <img src="https://img.wallpapersafari.com/desktop/1280/1024/66/92/G4sBWT.jpg"/>
        </div> 
      </>
    )
}
export default AboutUs;