function ClassesWeOffer() {
    return (
      <>
        <div className="c-block-text">
          <div className="container">
              <div className="c-textbox">Classes We Offer</div>
              <h3>Improve Your Game</h3>
          </div>
        </div>
        <div className="c-lessions-list">
            <ul className="clearfix">
              <li>
                <div className="c-lessions-img">
                    <img src="https://img.wallpapersafari.com/desktop/1280/1024/66/92/G4sBWT.jpg"/>
                    <div className="c-lessions-img__content">
                        <span className="c-lessions-img__title">Driving Lessons</span>
                        <span className="c-lessions-img__price">$700</span>
                        <a href="/">Schedule a Session</a>
                    </div>   
                  </div>
              </li>
              <li>
                <div className="c-lessions-img">
                  <img src="https://img.wallpapersafari.com/desktop/1280/1024/66/92/G4sBWT.jpg"/>
                    <div className="c-lessions-img__content">
                        <span className="c-lessions-img__title">Driving Lessons</span>
                        <span className="c-lessions-img__price">$700</span>
                        <a href="/">Schedule a Session</a>
                    </div>   
                  </div>
                </li> 
              </ul>
            </div>
        <div className="c-lession-bottom-img">
          <img src="https://img.wallpapersafari.com/desktop/1280/1024/66/92/G4sBWT.jpg"/>
          <div className="c-lession-bottom-img-content">
            <span className="c-lession-bottom-img-content__title">“The secret of getting ahead is getting started”</span>
            <span className="c-lession-bottom-img-content__author">Mark Twain</span>
          </div>
        </div>
      </>
    )
}
export default ClassesWeOffer;