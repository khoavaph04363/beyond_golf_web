import { Modal, Button, } from 'react-bootstrap';
import React, { useState } from 'react'

function OurServices(props) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function renderModal() {
        return <Modal show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}>
                
            <Modal.Header closeButton>
          <Modal.Title>Thanh Toán</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          I will not close if you click outside me. Don't even try to press
          escape key.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary">Understood</Button>
        </Modal.Footer>
        </Modal>
    }
    return (
        <>
            <div className="container">
                <div className="c-title-service">
                    <span>Our Secvices</span>
                </div>

                <div className="c-content-service">

                    <ul className="clearfix">
                        <li>
                            <div className="c-setup-service">
                                <div className="c-img-service">
                                    <img src="images/service1.webp" alt="" />
                                </div>
                                <div className="c-content-img">
                                    <p>Group Lessons</p>
                                </div>
                                <div className="c-btn-book">
                                    <button onClick={handleShow}>Book Now</button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="c-setup-service">
                                <div className="c-img-service">
                                    <img src="images/service1.webp" alt="" />
                                </div>
                                <div className="c-content-img">
                                    <p>Group Lessons</p>
                                </div>
                                <div className="c-btn-book">
                                    <button onClick={handleShow}>Book Now</button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="c-setup-service">
                                <div className="c-img-service">
                                    <img src="images/service1.webp" alt="" />
                                </div>
                                <div className="c-content-img">
                                    <p>Group Lessons</p>
                                </div>
                                <div className="c-btn-book">
                                    <button onClick={handleShow}>Book Now</button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="c-setup-service">
                                <div className="c-img-service">
                                    <img src="images/service1.webp" alt="" />
                                </div>
                                <div className="c-content-img">
                                    <p>Group Lessons</p>
                                </div>
                                <div className="c-btn-book">
                                    <button onClick={handleShow}>Book Now</button>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            {renderModal()}
        </>
    )

} export default OurServices;
